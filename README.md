Check out the [visualizer](https://kevinhinterlong.gitlab.io/cs126-astar-visualizer/)

This is just to help checking your program's output, it doesn't try to find the shortest path for you.